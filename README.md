## Amjad task
# overview
this code is a simple app to manage our daily tasks life, we can add, edit, delete and update
any todo in our data the update will be just for check or uncheck todo

## code structure
**Folder structure**
![folder_structure](assets/doc/img.png "folder_structure")

- **lib**: is the root folder for app and we will put all folders inside it

- **components** in this part we coding some widgets that common used to build ui

-  **core** in this folder  we put all parts to manage our app

- **config** in this folder we put all filles to help us to manage our project

- **routes** here we put all pages of the app

- **style** here we code all we need to styling what we need in our app

- **utils** in this folder we code all shared  functions we need in app

- **data** we put all folder we need for data (lacal storage or from network data )

- **models** models define the structure of the data used and convert from map to json

- **service**  used for tasks such as data fetching, business logic implementation, API interactions, state management, or any other operations that are not directly related to UI rendering.

- **fetures** here put all screens we need in our project, each fetures has two folders : **UI - LOGIC** inside every screen we have logic file and ui in logic file we code every  logicaly  implementation we need ,in ui we code every widget we need to build our app 


  