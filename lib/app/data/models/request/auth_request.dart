class LogInRequest {
  final String userName;
  final String password;

  LogInRequest({required this.userName, required this.password});

  Map<String, dynamic> get toJson =>
      {"username": userName, "password": password};
}
