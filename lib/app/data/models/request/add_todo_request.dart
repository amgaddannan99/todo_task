class AddTodoRequest{
  final String todo;
  final bool isCompleted;
  final int userId;

  AddTodoRequest({required this.todo, required this.isCompleted, required this.userId});
  Map<String, dynamic> get toJson =>
      {"todo": todo, "completed": isCompleted,"userId":userId};

}