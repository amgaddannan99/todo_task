import 'package:amjad_task/app/data/models/response/todo_model.dart';

class DeleteTodoModel extends TodoModel {
  final bool isDeleted;
  final DateTime deletedOn;

  DeleteTodoModel({
    required super.id,
    required super.todo,
    required super.completed,
    required super.userId,
    required this.isDeleted,
    required this.deletedOn,
  });

  factory DeleteTodoModel.fromJson(Map<String, dynamic> json) =>
      DeleteTodoModel(
        id: json["id"],
        todo: json["todo"],
        completed: json["completed"],
        userId: json["userId"],
        isDeleted: json["isDeleted"],
        deletedOn: DateTime.parse(json["deletedOn"]),
      );
}
