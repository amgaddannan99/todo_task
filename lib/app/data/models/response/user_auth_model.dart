import 'user_model.dart';

class UserAuthModel extends UserModel {
  final String token;

  UserAuthModel({
    required super.id,
    required super.username,
    required super.email,
    required super.firstName,
    required super.lastName,
    super.gender,
    super.image,
    required this.token,
  });

  factory UserAuthModel.fromJson(Map<String, dynamic> json) => UserAuthModel(
        id: json["id"],
        username: json["username"],
        email: json["email"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        gender: json["gender"],
        image: json["image"],
        token: json["token"],
      );
}
