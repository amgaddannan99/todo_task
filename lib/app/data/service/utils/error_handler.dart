import 'package:dio/dio.dart';

class NetworkException implements Exception {
  final ErrorCode errorCode;
  final dynamic error;
  final String? code;

  NetworkException(this.errorCode, this.error, this.code);

  @override
  String toString() => error;
}

enum ErrorCode {
  serverError,
  badRequest,
  unauthenticated,
  timeOut,
  noInternetConnection,
  unProcessableEntity,
  wrongInput,
  forbidden,
  conflict,
  connection,
  notFound
}

bool handleRemoteError(Response response) {
  final statusCode = response.statusCode ?? 0;
  final data = response.data;
  String? errorCode;
  if (data is Map<String, dynamic>) {
    errorCode = data["statusCode"].toString();
  }
  if (statusCode == 200 || statusCode == 201) {
    return true;
  }
  if (statusCode == 401) {
    throw NetworkException(
        ErrorCode.unauthenticated, data["error"] ?? data['message'], errorCode);
  } else if (statusCode == 403) {
    throw NetworkException(
        ErrorCode.forbidden, data["error"] ?? data['message'], errorCode);
  } else if (statusCode == 409) {
    throw NetworkException(
        ErrorCode.conflict, data["error"] ?? data['message'], errorCode);
  } else if (statusCode == 400) {
    throw NetworkException(
        ErrorCode.badRequest, data["error"] ?? data['message'], errorCode);
  } else if (statusCode == 404) {
    throw NetworkException(
        ErrorCode.badRequest, data["error"] ?? data['message'], errorCode);
  }
  throw NetworkException(ErrorCode.serverError, "SERVER_ERROR", errorCode);
}