int berPageLimit=10;

abstract class EndPoints{
  static const String baseUrl="https://dummyjson.com/";
  static const String logIn="auth/login";
  static const String viewTodosByUser="todos";
  static const String todos="todos";
  static const String addTodo="todos/add";
}