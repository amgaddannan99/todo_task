import 'package:amjad_task/app/data/models/response/user_auth_model.dart';

import '../../models/response/delete_todo_model.dart';
import '../../models/response/todo_model.dart';

class ResponseModel<T> {
  bool? succeeded;
  String? message;
  T data;
  Pagination? pagination;

  ResponseModel({
    required this.succeeded,
    required this.message,
    required this.data,
    required this.pagination,
  });

  static fromJson<T>(json) {
    dynamic map = {};
    if (json.containsKey("todos")) {
      map["data"] = json["todos"];
      map["meta"] = json;
    } else {
      map["data"] = json;
    }
    return ResponseModel(
      succeeded: map["succeeded"],
      message: map["message"],
      data: _converter<T>(map["data"] ?? <String, dynamic>{}),
      pagination: map["meta"] == null ? null : Pagination.fromJson(map["meta"]),
    );
  }

  static T _converter<T>(dynamic json) {
    if (json is Map<String, dynamic>) {
      if (T == UserAuthModel) {
        return UserAuthModel.fromJson(json) as T;
      }
      if (T == TodoModel) {
        return TodoModel.fromJson(json) as T;
      }
      if (T == DeleteTodoModel) {
        return DeleteTodoModel.fromJson(json) as T;
      }
    } else if (json is List) {
      if (<TodoModel>[] is T) {
        return List<TodoModel>.from(
            json.map((e) => TodoModel.fromJson(e))) as T;
      }
    }
    return json;
  }
}

class Pagination {
  final int total;
  final int skip;
  final int limit;
  final bool hasNextPage;

  Pagination({
    required this.total,
    required this.skip,
    required this.limit,
    required this.hasNextPage,
  });

  factory Pagination.fromJson(Map<String, dynamic> json) {
    return Pagination(
      total: json["total"],
      skip: json["skip"],
      limit: json["limit"],
      hasNextPage: ((json["total"] as int) ~/ (json["limit"] as int)) >
          ((json["skip"] as int)+1),
    );
  }

  @override
  toString(){
    return "total:$total, skip:$skip, limit:$limit, hasNext:$hasNextPage";
  }
}
