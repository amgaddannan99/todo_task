part of 'api_bloc.dart';

@immutable
abstract class ApiState {}

class ApiInitial extends ApiState {}

class Loading extends ApiState {}

class More extends ApiState {}

class Success<res> extends ApiState {
  final ResponseModel<res> data ;

  Success(this.data);
}

class Error extends ApiState {
  final String error;

  Error({ this.error="SomeThing went error"});
}
