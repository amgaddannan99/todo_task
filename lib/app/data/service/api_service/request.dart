enum MethodHttp {
  post,
  put,
  patch,
  delete,
  get;
}

class Request {
  final String endPoint;
  final Map<String, dynamic>? body;
  final MethodHttp method;
  final Map<String, dynamic>? headers;
  final Map<String, dynamic>? queryParameters;
  final bool withCache;

  Request({
    required this.endPoint,
    this.body,
    this.method=MethodHttp.get,
    this.headers,
    this.queryParameters,
    this.withCache=false,
  });

  Request copyWith({
    final Map<String, dynamic>? queryParameters,
  }) =>
      Request(
        endPoint: endPoint,
        body: body,
        method: method,
        queryParameters: queryParameters,
        headers: headers,
      );
}
