import 'dart:async';

import 'package:amjad_task/app/core/config/app_service.dart';
import 'package:amjad_task/app/data/service/api_service/request.dart';
import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

import '../../../core/utils/log.dart';
import '../utils/constants.dart';
import '../utils/error_handler.dart';
import '../utils/parser.dart';

part 'api_event.dart';

part 'api_state.dart';

/// this service to help us to manage requests when we want to fetch or request any endpoint and will help us to mange pagination
class ApiBloc<res> extends Bloc<ApiEvent, ApiState> {
  late Dio _dio;
  // user token
  late String _token;
  /// create an instance of [AppService] to get user token or any user info we need
  late AppService _appService;
  // to cancel any request we want or when class this service
  final CancelToken cancelToken = CancelToken();
  // to manage scrolling for pagination mode
  late ScrollController scrollController;
  /// to set target request data [endpoint], [httpMethod], [body], and [headers] if we want
  /// cache any response of that request we must set [withCache] to [true]
  late Request request;

  int skip = 0;
  bool haseNext = false;
  bool isGetMore = true;
  late var pagingData;

  ApiBloc() : super(ApiInitial()) {
    init();

    /// to call any request we have to use this event [ApiRequestEvent] and passing our request by it
    on<ApiRequestEvent>(
      (e, emit) async {
        request = e.request;
        await _requestApi(
          emit,
        );
      },
    );

    // those events to help us to remove, update or add any data from paging data
    on<RemoveFromPagingDataEvent>(_removeByIndexFromPagingData);
    on<UpdateItemInsidePagingEvent>(_updateItemInsideData);
    on<AddDataEvent>(_addDataToPagingData);
  }

  void init() {
    _appService = AppService();
    _token = _appService.token ?? "";
    _dio = Dio(
      BaseOptions(
        followRedirects: false,
        validateStatus: (status) => true,
        baseUrl: EndPoints.baseUrl,
        receiveTimeout: const Duration(seconds: 60),
        connectTimeout: const Duration(seconds: 100),
      ),
    );
    _dio.interceptors.add(
      LogInterceptor(
        responseBody: true,
        requestBody: true,
        responseHeader: true,
        requestHeader: true,
        request: true,
        error: true,
      ),
    );
  }

  /// this is required when have a pagination to set scroll controller and Listener
  void initPagination({
    required ScrollController scrollController,
  }) {
    this.scrollController = scrollController;
    this.scrollController.addListener(_setPagingListener);
  }

  /// to request api and handel response
  Future<void> _requestApi(Emitter<ApiState> emit) async {
    try {
      /// before emit any state we must know where we are if in pagination or normal request
      /// if skip is greater than 0 we are in pagination and we will get 2 page if skip is 0
      /// we will emit [Loading()] becose we do not have any data
      /// emit [More] when we will request new page
      if (skip > 0) {
        scrollController.jumpTo(scrollController.offset + 0.1);
        emit(More());
      } else {
        emit(Loading());
        // check if that endpoint has data or not
        if (request.withCache) {
          _getResponseFromCache(emit);
        }
      }
      /// get response for that request
      final response = await _performRequest(request);
      /// handel our response and check if it ok and status code is 200
      if (handleRemoteError(response)) {
        try {
          /// to parser our response to dart data for target [res]
          final ResponseModel<res> data =
              ResponseModel.fromJson<res>(response.data);
          if (data.pagination != null) {
            if (request.withCache && skip == 0) {
              _appService.box.write(request.endPoint, response.data);
            }
            _requestPagination(data.pagination!, data.data, emit);
          } else {
            if (request.withCache) {
              _appService.box.write(request.endPoint, response.data);
            }
            /// emit [data] to show it in ui or handel any action
            emit(Success<res>(data));
          }
        } catch (e) {
          // catching any error
          emit(Error());
          Log.logs(title: "ERROR ON {par}", msg: e.toString());
        }
      }
    } catch (e) {
      Log.logs(title: "ERROR ON {_requestApi}", msg: e.toString());
      emit(Error());
    }
  }

  /// to get response for any endpoint if that one hase data in storage
  Future<void> _getResponseFromCache(Emitter<ApiState> emit) async {
    final responseCache =
        _appService.box.read<Map<String, dynamic>>(request.endPoint);
    if (responseCache == null) return;
    try {
      final ResponseModel<res> data =
          ResponseModel.fromJson<res>(responseCache);
      if (data.pagination != null) {
        if (request.withCache && skip == 0) {
          _appService.box.write(request.endPoint, responseCache);
        }
        _requestPagination(data.pagination!, data.data, emit);
      } else {
        if (request.withCache) {
          _appService.box.write(request.endPoint, responseCache);
        }
        emit(Success<res>(data));
      }
    } catch (e) {
      emit(Error());
      Log.logs(title: "ERROR ON {par}", msg: e.toString());
    }
  }

  /// here to manage pagination and aggregate data
  Future<void> _requestPagination(
      Pagination pagination, res data, Emitter<ApiState> emit) async {
    Log.logs(title: "PAGE META IS", msg: pagination.toString());
    haseNext = pagination.hasNextPage;
    skip = pagination.skip;
    if (skip == 0) {
      pagingData = data;
    } else {
      pagingData!.addAll(data);
    }
    final ResponseModel<res> sendData = ResponseModel(
      data: pagingData,
      message: "",
      pagination: pagination,
      succeeded: true,
    );
    Log.logs(
      title: "DONE GET DATA FOR THIS PAGE $skip THE DATA IS",
      msg: "${pagingData.length}",
    );
    emit(Success<res>(sendData));
    isGetMore = true;
  }

  /// to perform our request and get data by request's httpMethode
  Future<Response> _performRequest(Request request) async {
    (request.headers ?? {})
        .forEach((key, value) => _dio.options.headers[key] = value);
    _dio.options.headers["Authorization"] =
        _token.isEmpty ? "" : "Bearer $_token";

    Connectivity connectivity = Connectivity();
    ConnectivityResult connectivityResult =
        await connectivity.checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      throw NetworkException(
        ErrorCode.noInternetConnection,
        "No internet",
        "",
      );
    }

    Map<String, dynamic> params = {};
    params.addAll(request.queryParameters ?? {});
    switch (request.method) {
      case MethodHttp.get:
        return _dio.get(
          request.endPoint,
          queryParameters: params,
          cancelToken: cancelToken,
        );
      case MethodHttp.post:
        return _dio.post(
          request.endPoint,
          data: request.body,
          queryParameters: params,
          cancelToken: cancelToken,
        );
      case MethodHttp.patch:
        return _dio.patch(
          request.endPoint,
          data: request.body,
          queryParameters: params,
          cancelToken: cancelToken,
        );
      case MethodHttp.put:
        return _dio.put(
          request.endPoint,
          data: request.body,
          queryParameters: params,
          cancelToken: cancelToken,
        );
      case MethodHttp.delete:
        return _dio.delete(
          request.endPoint,
          data: request.body,
          queryParameters: params,
          cancelToken: cancelToken,
        );
      default:
        throw UnsupportedError('Method not supported');
    }
  }

  _setPagingListener() {
    if ((scrollController.offset >= scrollController.position.maxScrollExtent &&
        !scrollController.position.outOfRange &&
        haseNext &&
        isGetMore)) {
      isGetMore = false;
      skip++;
      request = request.copyWith(
        queryParameters: {
          "limit": berPageLimit,
          "skip": skip,
        },
      );
      add(ApiRequestEvent(request: request));
    }
  }

  _removeByIndexFromPagingData(
      RemoveFromPagingDataEvent event, Emitter<ApiState> emit) {
    emit(Loading());
    try {
      pagingData.removeAt(event.index);
      final ResponseModel<res> sendData = ResponseModel(
        data: pagingData,
        message: "",
        pagination: null,
        succeeded: true,
      );
      Log.logs(
        title: "DELETE",
        msg: "${pagingData.length}",
      );
      emit(Success<res>(sendData));
    } catch (e) {
      Log.logs(
        title: "ERROR ON {_removeByIndexFromPagingData}",
        msg: e.toString(),
      );
      emit(Error());
    }
  }

  _updateItemInsideData(
      UpdateItemInsidePagingEvent event, Emitter<ApiState> emit) {
    emit(Loading());
    try {
      final ResponseModel<res> sendData = ResponseModel(
        data: pagingData,
        message: "",
        pagination: null,
        succeeded: true,
      );
      Log.logs(
        title: "Update",
        msg: "${pagingData.length}",
      );
      emit(Success<res>(sendData));
    } catch (e) {
      Log.logs(
        title: "ERROR ON {_updateItemInsideData}",
        msg: e.toString(),
      );
      emit(Error());
    }
  }

  _addDataToPagingData(AddDataEvent event, Emitter<ApiState> emit) {
    emit(Loading());
    try {
      pagingData.insert(0, event.data);
      final ResponseModel<res> sendData = ResponseModel(
        data: pagingData,
        message: "",
        pagination: null,
        succeeded: true,
      );
      Log.logs(
        title: "Add",
        msg: "${pagingData.length}",
      );
      emit(Success<res>(sendData));
    } catch (e) {
      Log.logs(
        title: "ERROR ON {_addDataToPagingData}",
        msg: e.toString(),
      );
      emit(Error());
    }
  }

  @override
  Future<void> close() async {
    cancelToken.cancel();
    Log.logs(title: "CLOSE", msg: "API REQUEST");
    super.close();
  }
}
