part of 'api_bloc.dart';

@immutable
abstract class ApiEvent {}

class ApiRequestEvent extends ApiEvent {
  final Request request;

  ApiRequestEvent({required this.request});
}

class RemoveFromPagingDataEvent extends ApiEvent {
  final int index;

  RemoveFromPagingDataEvent({required this.index});
}

class UpdateItemInsidePagingEvent extends ApiEvent {
}
class AddDataEvent extends ApiEvent {
  final dynamic data;

  AddDataEvent({required this.data});
}
