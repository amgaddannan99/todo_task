import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../components/button_widget.dart';
import '../../../core/utils/app_colors.dart';
import '../api_service/api_bloc.dart';
import '../utils/parser.dart';


/// this widget will help us to use a button for any request we want to call
/// [onDone] this func we can use it when get data
/// [onTap] this func when we want to call api or if we want to add any custom action
class ButtonRequestWidget<res> extends StatelessWidget {
  final String title;
  final double height;
  final double width;
  final double radius;
  final Function(ApiBloc<res> apiBloc) onTap;
  final Function(ResponseModel<res> data) onDone;

  const ButtonRequestWidget({
    super.key,
    required this.title,
    required this.radius,
    required this.onDone,
    required this.onTap, required this.height, required this.width,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ApiBloc<res>(),
      child: BlocBuilder<ApiBloc<res>, ApiState>(
        builder: (context, state) {
          if (state is Success<res>) {
            onDone(state.data);
          }
          return ButtonWidget(
            width: width,
            height: height,
            radius: radius,
            onPressed: () {
              onTap.call(context.read<ApiBloc<res>>());
            },
            text: title,
            fontSize: 20,
            isLoading: state is Loading,
            buttonColor: AppColors.APP_COLOR,
            fontColor: Colors.white,
          );
        },
      ),
    );
  }
}
