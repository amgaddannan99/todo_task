import 'package:amjad_task/app/components/text_widget.dart';
import 'package:amjad_task/app/data/service/api_service/request.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../api_service/api_bloc.dart';
import '../utils/constants.dart';


/// this service well help to manage pagination ui
/// to build each item in list we will use [builder] it hase context, index and data [data]each element of data response list
class ListViewPagination<res> extends StatefulWidget {
  final Widget Function(BuildContext context, int index, res data) builder;
  final Widget Function(BuildContext, int) separatorBuilder;
  final String endPoint;
  final ScrollController scrollController;

  const ListViewPagination({
    super.key,
    required this.builder,
    required this.separatorBuilder,
    required this.endPoint,
    required this.scrollController,
  });

  @override
  State<ListViewPagination<res>> createState() =>
      _ListViewPaginationState<res>();
}

class _ListViewPaginationState<res> extends State<ListViewPagination<res>> {
  late ApiBloc<List<res>> pageBloc;

  @override
  void initState() {
    final Request pageRequest = Request(
      endPoint: widget.endPoint,
      queryParameters: {
        "limit": berPageLimit,
        "skip": 0,
      },
      withCache: true,
    );
    pageBloc = context.read<ApiBloc<List<res>>>();
    pageBloc.initPagination(
      scrollController: widget.scrollController,
    );
    pageBloc.add(
      ApiRequestEvent(request: pageRequest),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ApiBloc<List<res>>, ApiState>(
      listener: (_, __) {},
      builder: (context, state) {
        if (state is Loading) {
          return const Center(
            child: CupertinoActivityIndicator(),
          );
        }
        if (state is Success<List<res>> || state is More) {

          return ListView.separated(
            controller: pageBloc.scrollController,
            padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
            itemBuilder: (context, index) {
              if (index == pageBloc.pagingData.length) {
                return const CupertinoActivityIndicator();
              }
              return widget.builder(
                context,
                index,
                pageBloc.pagingData[index],
              );
            },
            separatorBuilder: widget.separatorBuilder,
            itemCount: pageBloc.pagingData.length + (state is More ? 1 : 0),
          );
        }
        if (state is Error){
          return Center(child: TextWidget(state.error,weight: FontWeight.bold,size: 16,));
        }
        return const SizedBox();
      },
    );
  }
}
