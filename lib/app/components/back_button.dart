import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../core/utils/app_constanst.dart';

class BackButtonWidget extends StatelessWidget {
  final void Function() onTap;

  const BackButtonWidget({
    super.key,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return UnconstrainedBox(
      child: Hero(
        tag: AppConstants.BACK_HERO,
        child: Material(
          child: InkWell(
            onTap: onTap,
            child: Container(
              height: 40,
              width: 40,
              decoration: const BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                ],
              ),
              child: const UnconstrainedBox(
              child: Icon(Icons.arrow_back_ios_new_sharp),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
