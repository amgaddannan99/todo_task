import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_typedefs/rx_typedefs.dart';


class InkWellWidget extends StatelessWidget {
  final Color childColor;
  final Callback onTap;
  final Color splashColor;
  final double radius;
  final Widget child;
  final Size size;
  final BoxShape? shape;
  final EdgeInsetsGeometry? margin;
  final bool isCircle;
  final bool showShadow;
  final Border? border;

  const InkWellWidget({
    super.key,
    required this.childColor,
    required this.onTap,
    required this.splashColor,
    required this.radius,
    required this.child,
    required this.size,
    this.shape,
    this.margin,
    this.isCircle = false,
    this.showShadow = false,
    this.border ,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: isCircle
          ? BorderRadius.circular(radius)
          : BorderRadius.all(
        Radius.circular(radius),
      ),
      child: Material(
        color: childColor,
        child: InkWell(
          onTap: onTap,
          splashColor: splashColor,
          child: Container(
            margin: margin,
            height: size.height,
            width: size.width,
            decoration: BoxDecoration(
              // boxShadow:showShadow? [Get.find<AppTheme>().getAppShadow()]:null,

              borderRadius: isCircle
                  ? BorderRadius.circular(radius)
                  : BorderRadius.all(
                Radius.circular(radius),
              ),
              color: Colors.transparent,
              border: border,
              shape: shape ?? BoxShape.rectangle,
            ),
            child: Center(child: child),
          ),
        ),
      ),
    );
  }
}
