import 'package:amjad_task/app/core/style/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../core/utils/app_colors.dart';
import 'icon_button_widget.dart';
import 'text_widget.dart';

class TextFieldWidget extends StatelessWidget {
  const TextFieldWidget(
      {Key? key,
        this.upLabel,
        this.onDownTitleTap,
        this.initialValue,
        this.hint,
        this.prefix,
        this.prefixWidget,
        this.prefixIcon,
        this.suffixIcon,
        this.maxLength,
        this.onChange,
        this.onSuffixClicked,
        this.nextFocusNode,
        this.focusNode,
        this.validator,
        this.suffix,
        this.obscure = false,
        this.heightBetween = 10,
        this.height,
        this.textInputAction,
        this.justNumbers = false,
        this.enabled = true,
        this.textInputType,
        this.controller,
        this.ltr,
        this.maxLines,
        this.contentPadding,
        this.multiLines = false,
        this.onSubmitted,
        this.hideErrorText = false,
        this.suffixIconSize,
        this.textAlign = TextAlign.start,
        this.suffixIconColor,
        this.borderSideColor,
        this.suffixIconSvg,
        this.textStyle,
        this.autoValidateMode,
        this.hintTextStyle,
        this.autofillHints,
        this.autofocus = false})
      : super(key: key);

  final String? upLabel;
  final String? initialValue;
  final double heightBetween;
  final double? suffixIconSize;
  final EdgeInsets? contentPadding;
  final String? hint;
  final int? maxLength;
  final Color? suffixIconColor;
  final Color? borderSideColor;
  final int? maxLines;
  final IconData? suffixIcon;
  final String? suffixIconSvg;
  final TextEditingController? controller;
  final Widget? suffix;
  final Widget? prefix;
  final Widget? prefixWidget;
  final Widget? prefixIcon;
  final FocusNode? focusNode;
  final FocusNode? nextFocusNode;
  final Function(String)? onChange;
  final Function(String)? onSubmitted;
  final String? Function(String?)? validator;
  final Function()? onSuffixClicked;
  final Function()? onDownTitleTap;
  final TextAlign textAlign;
  final bool obscure;
  final bool enabled;
  final TextInputAction? textInputAction;
  final TextInputType? textInputType;
  final bool justNumbers;
  final bool? ltr;
  final bool multiLines;
  final bool hideErrorText;
  final bool? autofocus;
  final TextStyle? textStyle;
  final TextStyle? hintTextStyle;
  final AutovalidateMode? autoValidateMode;
  final double? height;
  final List<String>? autofillHints;
  @override
  Widget build(BuildContext context) {
    // final ltr = this.ltr ?? Get.find<AppBuilder>().locale.isEnglish;
    final sufIcon = suffix ??
        (suffixIcon != null || suffixIconSvg != null
            ? Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: SizedBox(
            height: 40,
            width: 40,
            child: IconButtonWidget(
              icon: suffixIcon,
              svg: suffixIconSvg,
              color: suffixIconColor,
              iconColor: suffixIconColor,
              iconSize: suffixIconSize,
              onPressed: onSuffixClicked,
            ),
          ),
        )
            : null);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (upLabel != null)
          Padding(
            padding:
            EdgeInsetsDirectional.only(bottom: heightBetween, start: 16),
            child: Row(
              children: [
                TextWidget(
                  upLabel!,
                  textStyle: context.h3s12cb,
                ),
              ],
            ),
          ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (prefixWidget != null) prefixWidget!,
            Expanded(
              // child: Directionality(
              // textDirection: ltr ? TextDirection.ltr : TextDirection.rtl,
              child: SizedBox(
                child: TextFormField(
                  controller: controller,
                  autofocus: autofocus!,
                  validator: validator,
                  maxLength: maxLength,
                  expands: multiLines,
                  autovalidateMode: autoValidateMode,
                  initialValue: initialValue,
                  onChanged: onChange,
                  textAlign: textAlign,
                  onFieldSubmitted: onSubmitted,
                  focusNode: focusNode,
                  enabled: enabled,
                  maxLines: multiLines ? null : (maxLines ?? 1),
                  onEditingComplete: nextFocusNode == null
                      ? null
                      : () {
                    FocusScope.of(context).requestFocus(nextFocusNode);
                  },
                  textInputAction: textInputAction,
                  style: textStyle ?? context.h2S16,
                  cursorColor:
                 AppColors.APP_COLOR,
                  keyboardType: textInputType,
                  obscureText: obscure,
                  inputFormatters: [
                    if (justNumbers) FilteringTextInputFormatter.digitsOnly
                  ],
                  autofillHints: autofillHints,
                  contextMenuBuilder: (context, editableTextState) {
                    final List<ContextMenuButtonItem> buttonItems =
                        editableTextState.contextMenuButtonItems;
                    return AdaptiveTextSelectionToolbar.buttonItems(
                      anchors: editableTextState.contextMenuAnchors,
                      buttonItems: buttonItems,
                    );
                  },
                  textAlignVertical: TextAlignVertical.center,
                  decoration: InputDecoration(
                    hintText: hint?.tr,
                    counterText: "",
                    errorStyle: context.e1S11W500,
                    // errorStyle: hideErrorText
                    //     ? TextStyle(
                    //         // fontFamily: FontFamily.montserrat,
                    //         // color: Colors.black,
                    //         fontSize: 10,
                    //       )
                    //     : null,
                    constraints: height == null
                        ? null
                        : BoxConstraints(maxHeight: height!),
                    fillColor: AppColors.APP_COLOR,
                    filled: false,
                    contentPadding: const EdgeInsetsDirectional.only(
                        start: 15, top: 17, bottom: 17),
                    // isCollapsed: true,
                    hintStyle: hintTextStyle ?? context.hint1S16W500,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(
                       10,
                      ),
                      borderSide: BorderSide(
                        color:AppColors.APP_COLOR,
                        width: 1,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: AppColors.APP_COLOR,
                        width: 1,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(
                     10,
                      ),
                      borderSide: BorderSide(
                        color: Colors.grey.withOpacity(.2),
                        width: 1,
                      ),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: AppColors.ERROR_COLOR,
                        width: 1,
                      ),
                    ),
                    prefix: prefix,
                    prefixIconConstraints: const BoxConstraints(
                      maxHeight: 40,
                      minWidth: 40,
                    ),
                    prefixIcon: prefixIcon,
                    suffixIconConstraints: const BoxConstraints(
                      maxHeight: 40,
                      minHeight: 20,
                    ),
                    suffixIcon: sufIcon,
                  ),
                ),
              ),
            ),
            // ),
          ],
        ),

      ],
    );
  }
}
