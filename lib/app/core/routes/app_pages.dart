import 'package:amjad_task/app/data/service/api_service/api_bloc.dart';
import 'package:amjad_task/app/features/auth/log_in/logic/log_in_bloc.dart';
import 'package:amjad_task/app/features/home/add_tasks/logic/add_tasks_bloc.dart';
import 'package:amjad_task/app/features/home/add_tasks/ui/add_tasks.dart';
import 'package:amjad_task/app/features/home/home_view/logic/home_bloc.dart';
import 'package:amjad_task/app/features/home/home_view/ui/home_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/models/response/delete_todo_model.dart';
import '../../data/models/response/todo_model.dart';
import '../../features/auth/log_in/ui/log_in.dart';
import '../../features/splash/ui/splash_screen.dart';
import '../config/app_service.dart';
import 'app_routes.dart';


/// it is a core for all app pages we will navigat and here we will provide any bloc for target page
class AppPages {
  Route generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.loginScreen:
        {
          return MaterialPageRoute(
            builder: (_) => BlocProvider(
              create: (context) => LogInBloc(),
              child: const LogInView(),
            ),
          );
        }
      case Routes.homeScreen:
        {
          return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (context) => HomeBloc(),
                ),
                BlocProvider(
                  create: (context) => ApiBloc<List<TodoModel>>(),
                ),
                BlocProvider(
                  create: (context) => ApiBloc<DeleteTodoModel>(),
                ),
                BlocProvider(
                  create: (context) => ApiBloc<TodoModel>(),
                ),
              ],
              child: const HomeView(),
            ),
          );
        }
      case Routes.addTasksScreen:
        {
          return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (context) => AddTasksBloc(),
                ),
                BlocProvider(
                  create: (context) => ApiBloc<TodoModel>(),
                ),
              ],
              child: AddTasksView(),
            ),
          );
        }

      default:
        {
          return MaterialPageRoute(
            builder: (_) => BlocProvider(
              create: (context) => AppService(),
              child: SplashScreen(),
            ),
          );
        }
    }
  }
}
