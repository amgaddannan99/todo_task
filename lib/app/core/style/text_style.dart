import 'package:flutter/material.dart';

import '../utils/app_colors.dart';

extension TextStyleExtension on BuildContext {
  TextStyle get h3s12cb => Theme.of(this).textTheme.titleSmall!;
  TextStyle get h2S16 => Theme.of(this).textTheme.titleMedium!;
  ///[CUSTOM FONTS]///
  TextStyle get e1S11W500 => Theme.of(this).textTheme.titleMedium!.copyWith(
    fontSize: 11,
    color: AppColors.ERROR_COLOR,
  );

  TextStyle get e1S16W500 => Theme.of(this).textTheme.titleMedium!.copyWith(
    fontSize: 16,
    color: AppColors.ERROR_COLOR,
  );

  TextStyle get hint1S16W500 => Theme.of(this).textTheme.titleMedium!.copyWith(
    fontSize: 16,
    color: Colors.grey.withOpacity(.3)
  );
}
