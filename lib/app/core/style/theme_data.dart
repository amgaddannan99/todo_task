import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../utils/app_colors.dart';

abstract class AppThemeData {
  static ThemeData darkTheme = ThemeData();

  static ThemeData lightTheme = ThemeData(
    useMaterial3: true,
    scaffoldBackgroundColor: AppColors.PRAIMARY,
    appBarTheme: AppBarTheme(
      backgroundColor: AppColors.PRAIMARY,
      surfaceTintColor: Colors.transparent,
      titleTextStyle: const TextStyle(
          color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
      centerTitle: false,
      titleSpacing: 0.0,
    ),
    textTheme: const TextTheme(
      titleLarge: TextStyle(
        fontSize: 28,
        fontWeight: FontWeight.bold,
      ),
      titleMedium: TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.w600,
      ),
      titleSmall: TextStyle(
        fontSize: 15,
        fontWeight: FontWeight.normal,
      ),
    ),
  );
}
