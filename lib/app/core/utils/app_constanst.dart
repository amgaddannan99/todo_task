class AppConstants {
  static const String IMAGE_ASSETS_PATH = "assets/media/images/";
  static const String ICON_ASSETS_PATH = "assets/icons/";
  static const double RADIUS = 10;
  static const String BUTTON_HERO = "button_tag";
  static const String BACK_HERO = "back_tag";
}