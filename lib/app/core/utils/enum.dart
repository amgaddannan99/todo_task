import 'package:amjad_task/app/core/utils/app_colors.dart';
import 'package:flutter/material.dart';

enum ToastState{
success,
  warring,
  error;
  Color get colorState => switch (this) {
    success => AppColors.SUCCESS_COLOR,
    warring => AppColors.WARINING_COLOR,
    error => AppColors.ERROR_COLOR,
  };
}