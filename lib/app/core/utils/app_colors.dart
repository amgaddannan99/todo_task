import 'dart:ui';

import 'package:flutter/material.dart';

class AppColors {
  AppColors._();
  static const Color APP_COLOR = Color(0xffFC7677);
  static Color PRAIMARY = Color(0xffFFFFFF);
  static Color ERROR_COLOR = Color(0xffF54336);
  static Color SUCCESS_COLOR = Colors.lightGreenAccent;
  static Color WARINING_COLOR = Colors.orange;
  static const Color GREEN = Color(0xff4BAE4F);
  static const Color ORANG_COLOR = Color(0xffEB6440);


}
