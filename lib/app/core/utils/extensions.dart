import 'package:flutter/cupertino.dart';

import 'log.dart';

extension NonNullString on String? {
  String get orEmpty {
    if (this == null) {
      return "";
    } else {
      return this!;
    }
  }
}
extension StringExtension on String {
  String get getFirstAndLast =>
      isEmpty
          ? "**"
          : trim().contains(" ")
          ? "${this[0]} ${this[indexOf(" ") + 1]}".toUpperCase()
          : this[0].toUpperCase();
}
extension Navigation on BuildContext {
  ////[NAVIGATOR]////
  Future<dynamic> pushNamed(String route, {Object? arg}) {
    Log.logs(title: "PUSH", msg: ":{$route}:");
    return Navigator.of(this).pushNamed(
      route,
      arguments: arg,
    );
  }

  Future<dynamic> pushReplacementNamed(String route, {Object? arg}) {
    Log.logs(title: "PUSH", msg: ":{$route}:");
    return Navigator.of(this).pushReplacementNamed(
      route,
      arguments: arg,
    );
  }

  Future<dynamic> pushNamedAndRemoveUntil(String route, {
    Object? arg,
    required RoutePredicate predicate,
  }) {
    Log.logs(title: "PUSH", msg: ":{$route}:");
    return Navigator.of(this).pushNamedAndRemoveUntil(
      route,
      predicate,
      arguments: arg,
    );
  }

  void pop<T>({T? res}) {
    Log.logs(title: "POP", msg: ":{}:");
    return Navigator.of(this).pop<T>(
      res,
    );
  }

  ////[RESPONSE]////

  bool get isSmall {
    return MediaQuery
        .of(this)
        .size
        .width < 1500;
  }

  double get screenHeight {
    return MediaQuery
        .of(this)
        .size
        .height;
  }

  double get screenWidth {
    return MediaQuery
        .of(this)
        .size
        .width;
  }
}

extension SizeBoxEx on SizedBox{
  Widget get appWidth20 => const SizedBox(width: 20,);
  Widget get appHeight20 => const SizedBox(height: 20,);
}