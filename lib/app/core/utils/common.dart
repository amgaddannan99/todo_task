import 'package:amjad_task/app/core/utils/enum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';

import 'package:fluttertoast/fluttertoast.dart';

import '../../components/text_widget.dart';

/// This class to add all common func will used in my code
abstract class Common {
  static void initStatusBars({
    required Color color,
  }) {
    FlutterStatusbarcolor.setStatusBarColor(color, animate: true);
    FlutterStatusbarcolor.setNavigationBarColor(color, animate: true);
  }

  /// when want to show a message to user we have three states [success, warring and error] to set message state by using [state] var from [ToastState] enum
  static showToast(
      {required String message,
      required ToastState state,
      required BuildContext context}) {
    FToast fToast;
    fToast = FToast();
    fToast.init(context);
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: state.colorState.withOpacity(.5),
        boxShadow: const [
          BoxShadow(
              color: Colors.black12,
              offset: Offset.zero,
              spreadRadius: 1,
              blurRadius: 5)
        ],
      ),
      child: TextWidget(message,
          textColor: state.colorState, textAlign: TextAlign.center),
    );
    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: const Duration(seconds: 3),
    );
  }
}
