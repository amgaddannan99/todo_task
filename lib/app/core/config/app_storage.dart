import 'dart:convert';

import 'package:amjad_task/app/core/utils/log.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppStorage {
  late SharedPreferences _prefs;

  AppStorage() {
    _initPrefs();
  }

  Future<void> _initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
  }

  T? read<T>(String key) {
    if (_prefs.containsKey(key)) {
      if (T == bool) {
        return _prefs.getBool(key) as T?;
      } else if (T == int) {
        return _prefs.getInt(key) as T?;
      } else if (T == double) {
        return _prefs.getDouble(key) as T?;
      } else if (T == String) {
        return _prefs.getString(key) as T?;
      } else if (T == Map<String, dynamic>) {
        final jsonString = _prefs.getString(key);
        if (jsonString != null) {
          return json.decode(jsonString) as T?;
        }
      }
    }
    return null;
  }

  Future<void> write<T>(String key, T value) async {
    Log.logs(title: "WRITE DATA FOR KEY $key", msg: value.toString());
    if (value is bool) {
      await _prefs.setBool(key, value);
    } else if (value is int) {
      await _prefs.setInt(key, value);
    } else if (value is double) {
      await _prefs.setDouble(key, value);
    } else if (value is String) {
      await _prefs.setString(key, value);
    } else if (value is Map<String, dynamic>) {
      final jsonString = json.encode(value);
      await _prefs.setString(key, jsonString);
    }
  }

  bool containsKey(String key) {
    return _prefs.containsKey(key);
  }

  Future<void> remove(String key) async {
    await _prefs.remove(key);
  }

  Future<void> removeAll() async {
    await _prefs.clear();
  }
}
