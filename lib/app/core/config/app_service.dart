import 'package:amjad_task/app/core/utils/log.dart';
import 'package:amjad_task/app/data/models/response/user_model.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../data/models/response/user_auth_model.dart';
import 'app_storage.dart';

@immutable
abstract class AppServiceEvent {}

class InitAppEvent extends AppServiceEvent {}

@immutable
abstract class AppServiceState {}

class InitApp extends AppServiceState {}

/// [AppService] is a core of our code we but here all app config like [token], [user], and etc..
class AppService extends Bloc<AppServiceEvent, AppServiceState> {
  static final AppService _instance = AppService._internal();

  factory AppService() {
    return _instance;
  }

  String? token;
  UserModel? user;

  /// this is to set any data to storage
  AppStorage box = AppStorage();

  final _secureStorage = const FlutterSecureStorage();

  AppService._internal() : super(InitApp()) {
    on<AppServiceEvent>(
      (_, __) {
        loadData();
      },
    );
  }

  /// here to load user data and we call it everytime app running
  loadData() async {
    Map<String, dynamic> data = await _secureStorage.readAll();
    if (data.keys.contains("token")) {
      token = data["token"];
    }
    if (box.containsKey("User")) {
      user = UserModel.fromJson(box.read<Map<String, dynamic>>("User") ?? {});
    }
    Log.logs(title: "TOKEN", msg: token ?? "");
    Log.logs(title: "USET ID", msg: user?.id.toString() ?? "");
  }

  /// when set user data like token an user info(name, email,..)
  setUserData({
    required UserAuthModel user,
  }) {
    _setToken(user.token);
    _setUserInfo(user);
  }

  /// to set token of user when we used [_secureStorage] to secure it when cache it
  _setToken(String? token) {
    if (token == null) {
      _secureStorage.delete(key: "token");
      this.token = null;
      return;
    }
    _secureStorage.write(key: "token", value: token);
    this.token = token;
  }

  /// to cache user data
  _setUserInfo(UserModel? user) {
    box.write<Map<String, dynamic>?>("User", user?.toJson());
    this.user = user;
  }


}
