import 'package:amjad_task/app/core/utils/extensions.dart';
import 'package:flutter/material.dart';

import '../../../core/config/app_service.dart';
import '../../../core/routes/app_routes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    final appServiceBloc = AppService();
    appServiceBloc.add(InitAppEvent());
    Future.delayed(
      const Duration(seconds: 4),
      () {
        if (appServiceBloc.token == null) {
          context.pushReplacementNamed(Routes.loginScreen);
        } else {
          context.pushReplacementNamed(Routes.homeScreen);
        }
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text("HI"),
      ),
    );
  }
}
