import 'package:amjad_task/app/core/utils/extensions.dart';
import 'package:amjad_task/app/core/style/text_style.dart';
import 'package:amjad_task/app/features/home/add_tasks/logic/add_tasks_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../components/back_button.dart';
import '../../../../components/text_widget.dart';
import '../../../../data/models/response/todo_model.dart';
import '../../../../data/service/api_service/api_bloc.dart';

class AddTasksView extends StatefulWidget {
  AddTasksView({super.key});

  @override
  State<AddTasksView> createState() => _AddTasksViewState();
}

class _AddTasksViewState extends State<AddTasksView> {
  late AddTasksBloc bloc;

  @override
  void initState() {
    bloc = context.read<AddTasksBloc>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<ApiBloc<TodoModel>, ApiState>(
        listener: (context, state) {
          if (state is Loading) {
            FocusScope.of(context).unfocus();
            showDialog(
              context: context,
              builder: (context) => Container(
                  color: Colors.white,
                  child: const Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextWidget("Adding.....",weight: FontWeight.bold,size: 20),
                      SizedBox(height: 20,),
                      CupertinoActivityIndicator(),
                    ],
                  )
              ),
            );
          }
          if (state is Success<TodoModel>) {
            context.pop();
            context.pop(res: state.data.data);
          }
          if (state is Error) {
            context.pop();
          }
        },
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              actions: [
                BlocConsumer<AddTasksBloc, AddTasksState>(
                  builder: (context, state) {
                    if (state is EmptyOrNotSate) {
                      return !state.isEmpty
                          ? IconButton(
                        onPressed: () {
                          bloc.addTask(context.read<ApiBloc<TodoModel>>());
                        },
                        icon: const Icon(Icons.check),
                      )
                          : const SizedBox();
                    }
                    return const SizedBox();
                  },
                  listener: (_, __) {},
                ),
              ],
              leading: BackButtonWidget(onTap: () {}),
            ),
            body: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: TextField(
                    autofocus: true,
                    maxLines: 10,
                    controller: bloc.toDoController,
                    textInputAction: TextInputAction.newline,
                    decoration: InputDecoration(
                        hintStyle: context.hint1S16W500,
                        hintText: "Start typing",
                        enabledBorder: const UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        focusedBorder: const UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent))),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
