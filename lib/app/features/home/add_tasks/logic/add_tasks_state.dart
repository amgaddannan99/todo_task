part of 'add_tasks_bloc.dart';

@immutable
abstract class AddTasksState {}

class AddTasksInitial extends AddTasksState {}

class EmptyOrNotSate extends AddTasksState {
  final bool isEmpty;

  EmptyOrNotSate({required this.isEmpty});
}
