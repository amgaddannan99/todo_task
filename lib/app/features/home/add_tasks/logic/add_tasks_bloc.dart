import 'dart:async';

import 'package:amjad_task/app/core/config/app_service.dart';
import 'package:amjad_task/app/data/models/request/add_todo_request.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

import '../../../../data/service/api_service/api_bloc.dart';
import '../../../../data/service/api_service/request.dart';
import '../../../../data/service/utils/constants.dart';

part 'add_tasks_event.dart';

part 'add_tasks_state.dart';

class AddTasksBloc extends Bloc<AddTasksEvent, AddTasksState> {
  TextEditingController toDoController = TextEditingController();

  AddTasksBloc() : super(AddTasksInitial()) {
    toDoController.addListener(
      () {
        emit(EmptyOrNotSate(isEmpty: toDoController.text.isEmpty));
      },
    );
  }

  Request get _addTaskReq => Request(
        method: MethodHttp.post,
        endPoint: EndPoints.addTodo,
        body: AddTodoRequest(
          todo: toDoController.text.trim(),
          isCompleted: false,
          userId: AppService().user!.id,
        ).toJson,
      );

  void addTask<TodoModel>(ApiBloc<TodoModel> apiBloc) {
    apiBloc.add(
      ApiRequestEvent(request: _addTaskReq),
    );
  }

  @override
  Future<void> close() {
    toDoController.dispose();
    return super.close();
  }
}
