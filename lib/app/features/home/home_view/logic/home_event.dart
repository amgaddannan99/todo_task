part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class ViewTodosEvent extends HomeEvent {}

class TodoEvent<T> extends ViewTodosEvent {
  final int toDoId;
  final ApiBloc<T> apiBloc;
  final MethodHttp type;

  TodoEvent({
    required this.toDoId,
    required this.apiBloc,
    required this.type,
  });
}
