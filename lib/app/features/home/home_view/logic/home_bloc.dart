import 'package:amjad_task/app/data/service/api_service/api_bloc.dart';
import 'package:amjad_task/app/data/service/api_service/request.dart';
import 'package:amjad_task/app/data/service/utils/constants.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import '../../../../data/models/response/todo_model.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  int selectedId = -1;

  HomeBloc() : super(HomeInitial()) {
    on<HomeEvent>(
      (event, emit) {},
    );
    on<TodoEvent>(
      (event, emit) {
        _todoRequest(event);
      },
    );
  }

  _todoRequest(TodoEvent event) {
    selectedId = event.toDoId;
    Request deleteRequest = Request(
      endPoint: "${EndPoints.todos}/${event.toDoId}",
      method: event.type,
    );
    event.apiBloc.add(
      ApiRequestEvent(request: deleteRequest),
    );
  }

  removeTaskFromPaging(ApiBloc<List<TodoModel>> pagingBloc) {
    List<TodoModel> todos = pagingBloc.pagingData;
    final int i = todos.indexWhere((element) => element.id == selectedId);
    pagingBloc.add(
      RemoveFromPagingDataEvent(index: i),
    );
  }

  updateTask(ApiBloc<List<TodoModel>> pagingBloc) {
    List<TodoModel> todos = pagingBloc.pagingData;
    int index = todos.indexWhere((element) => element.id == selectedId);
    if (index != -1) {
      TodoModel updatedItem =
          todos[index].copyWith(completed: !todos[index].completed);
      todos[index] = updatedItem;
      pagingBloc.add(UpdateItemInsidePagingEvent());
    }
  }
}
