import 'package:amjad_task/app/components/text_widget.dart';
import 'package:amjad_task/app/core/utils/extensions.dart';
import 'package:amjad_task/app/core/routes/app_routes.dart';
import 'package:amjad_task/app/core/utils/app_colors.dart';
import 'package:amjad_task/app/data/service/utils/constants.dart';
import 'package:amjad_task/app/features/home/home_view/logic/home_bloc.dart';
import 'package:amjad_task/app/features/home/home_view/ui/widgets/card_todos.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/models/response/todo_model.dart';
import '../../../../data/service/api_service/api_bloc.dart';
import '../../../../data/service/builder/list_view_pagination.dart';

class HomeView extends StatefulWidget {
  const HomeView({
    super.key,
  });

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  late HomeBloc bloc;

  @override
  void initState() {
    bloc = context.read<HomeBloc>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final TodoModel? data =
              await context.pushNamed(Routes.addTasksScreen);
          if (data != null) {
            context.read<ApiBloc<List<TodoModel>>>().add(AddDataEvent(data: data));
          }
        },
        backgroundColor: AppColors.APP_COLOR,
        child: const Icon(Icons.add),
      ),
      appBar: AppBar(title: const TextWidget("Todos"), centerTitle: true),
      body: Column(
        children: [
          Expanded(
            child: ListViewPagination<TodoModel>(

              builder: (context, index, data) {
                return CardTodos(
                  todosResponse: data,
                  bloc: bloc,
                );
              },
              endPoint: EndPoints.viewTodosByUser,
              scrollController: ScrollController(),
              separatorBuilder: (_, __) => const SizedBox(
                height: 15,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
