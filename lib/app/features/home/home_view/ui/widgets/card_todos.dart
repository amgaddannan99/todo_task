import 'package:amjad_task/app/components/text_widget.dart';
import 'package:amjad_task/app/core/utils/extensions.dart';
import 'package:amjad_task/app/data/models/response/todo_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../components/ink_well_widget.dart';
import '../../../../../core/utils/app_colors.dart';
import '../../../../../data/models/response/delete_todo_model.dart';
import '../../../../../data/service/api_service/api_bloc.dart';
import '../../../../../data/service/api_service/request.dart';
import '../../logic/home_bloc.dart';

class CardTodos extends StatelessWidget {
  final TodoModel todosResponse;
  final HomeBloc bloc;

  const CardTodos({
    super.key,
    required this.todosResponse,
    required this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      constraints: const BoxConstraints(
        minHeight: 60
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: AppColors.APP_COLOR,
          width: .3
        ),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          BlocConsumer<ApiBloc<DeleteTodoModel>, ApiState>(
            listener: (context, state) {
              if (state is Loading) {
                showDialog(
                  context: context,
                  builder: (context) => Container(
                      color: Colors.white,
                      child: const Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextWidget("Deleting.....",weight: FontWeight.bold,size: 20),
                          SizedBox(height: 20,),
                          CupertinoActivityIndicator(),
                        ],
                      )
                  ),
                );
              }
              if (state is Success<DeleteTodoModel>) {
                context.pop();
                bloc.removeTaskFromPaging(
                    context.read<ApiBloc<List<TodoModel>>>());
              }
              if (state is Error) {
                context.pop(

                );
              }
            },
            listenWhen: (p, c) {
              return todosResponse.id == bloc.selectedId;
            },
            buildWhen: (p, c) {
              return todosResponse.id == bloc.selectedId;
            },
            builder: (context, state) {
              return InkWellWidget(
                childColor: Colors.grey.withOpacity(.1),
                onTap: () {
                  bloc.add(
                    TodoEvent<DeleteTodoModel>(
                      toDoId: todosResponse.id,
                      apiBloc: context.read<ApiBloc<DeleteTodoModel>>(),
                      type: MethodHttp.delete,
                    ),
                  );
                },
                splashColor: Colors.red,
                radius: 10,
                size: const Size(35, 35),
                child: const Icon(
                  Icons.delete,
                  color: Colors.red,
                ),
              );
            },
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(child: Text(todosResponse.todo)),
          const SizedBox(
            width: 10,
          ),
          BlocConsumer<ApiBloc<TodoModel>, ApiState>(
            listener: (context, state) {
              if (state is Loading) {
                showDialog(
                  context: context,
                  builder: (context) => Container(
                    color: Colors.white,
                    child: const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextWidget("Updating.....",weight: FontWeight.bold,size: 20),
                        SizedBox(height: 20,),
                        CupertinoActivityIndicator(),
                      ],
                    )
                  ),
                );
              }
              if (state is Success<TodoModel>) {
                context.pop();
                bloc.updateTask(context.read<ApiBloc<List<TodoModel>>>());
              }
              if (state is Error) {
                context.pop();
              }
            },
            listenWhen: (p, c) {
              return todosResponse.id == bloc.selectedId;
            },
            buildWhen: (p, c) {
              return todosResponse.id == bloc.selectedId;
            },
            builder: (context, state) {
              return InkWellWidget(
                childColor: Colors.grey.withOpacity(.1),
                onTap: () {
                  bloc.add(
                    TodoEvent<TodoModel>(
                      toDoId: todosResponse.id,
                      apiBloc: context.read<ApiBloc<TodoModel>>(),
                      type: MethodHttp.put,
                    ),
                  );
                },
                splashColor: Colors.green,
                radius: 10,
                size: const Size(35, 35),
                child: Checkbox(
                  activeColor: AppColors.GREEN,
                  value: todosResponse.completed,
                  onChanged: (val) {
                    bloc.add(
                      TodoEvent<TodoModel>(
                        toDoId: todosResponse.id,
                        apiBloc: context.read<ApiBloc<TodoModel>>(),
                        type: MethodHttp.put,
                      ),
                    );
                  },
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
