import 'package:amjad_task/app/core/utils/extensions.dart';
import 'package:amjad_task/app/core/utils/app_validator.dart';
import 'package:amjad_task/app/data/models/response/user_auth_model.dart';
import 'package:amjad_task/app/features/auth/log_in/logic/log_in_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../components/text_field_widget.dart';
import '../../../../components/text_widget.dart';
import '../../../../core/routes/app_routes.dart';
import '../../../../data/service/builder/button_builder.dart';

class LogInView extends StatefulWidget {
  const LogInView({super.key});

  @override
  State<LogInView> createState() => _LogInViewState();
}

class _LogInViewState extends State<LogInView> {
  late LogInBloc bloc;

  @override
  void initState() {
    bloc = context.read<LogInBloc>();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Container(
          width: context.screenWidth,
          margin: const EdgeInsetsDirectional.symmetric(horizontal: 20),
          child: Form(
            key: bloc.formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 60,
                ),
                TextWidget(
                  "Welcome",
                  textStyle: Theme.of(context).textTheme.titleLarge,
                  children: const [TextWidget(" ... ")],
                ),
                const SizedBox(
                  height: 15,
                ),
                TextWidget("enter your user name and password to log in",
                    textStyle: Theme.of(context).textTheme.titleSmall),
                const SizedBox(
                  height: 65,
                ),
                TextWidget("enter your user name",
                    textStyle: Theme.of(context).textTheme.titleSmall),
                const SizedBox(
                  height: 5,
                ),
                TextFieldWidget(
                  controller: bloc.textEditingControllerUserName,
                  textInputType: TextInputType.text,
                  ltr: true,
                  hint: "User Name",
                  validator: AppValidator.validator,
                  textAlign: TextAlign.left,
                ),
                const SizedBox(
                  height: 30,
                ),
                TextWidget("enter your password ",
                    textStyle: Theme.of(context).textTheme.titleSmall),
                const SizedBox(
                  height: 5,
                ),
                BlocBuilder<LogInBloc, LogInState>(
                  builder: (context, state) {
                    bool hidePassword = true;
                    if (state is HidePasswordState) {
                      hidePassword = state.isHide;
                    }
                    return TextFieldWidget(
                      controller: bloc.textEditingControllerPassword,
                      textInputType: TextInputType.text,
                      ltr: true,
                      hint: "Password",
                      validator: AppValidator.validator,
                      textAlign: TextAlign.left,
                      obscure: hidePassword,
                      suffixIcon: hidePassword
                          ? Icons.visibility_off
                          : Icons.visibility,
                      onSuffixClicked: () {
                        context.read<LogInBloc>().add(
                              HidePasswordEvent(),
                            );
                      },
                    );
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                Center(
                  child: ButtonRequestWidget<UserAuthModel>(
                    height: 50,
                    width: 400,
                    radius: 10,
                    title: "Login",
                    onTap: bloc.login,
                    onDone: (data) {
                      bloc.onLoginSuccess(data.data,context);
                    },
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
