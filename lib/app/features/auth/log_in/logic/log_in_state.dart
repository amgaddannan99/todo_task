part of 'log_in_bloc.dart';

@immutable
abstract class LogInState {}

class LogInInitial extends LogInState {}

class HidePasswordState extends LogInState{
  final bool isHide;

  HidePasswordState(this.isHide);
}
class LoadingState extends LogInState{}
class SuccessState extends LogInState{}
class ErrorState extends LogInState{}
