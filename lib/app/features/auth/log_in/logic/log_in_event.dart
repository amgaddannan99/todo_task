part of 'log_in_bloc.dart';

@immutable
abstract class LogInEvent {}
class HidePasswordEvent extends LogInEvent{}
class OnLogInEvent extends LogInEvent{}
