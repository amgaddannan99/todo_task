import 'dart:async';
import 'package:amjad_task/app/core/utils/extensions.dart';
import 'package:amjad_task/app/core/routes/app_routes.dart';
import 'package:amjad_task/app/data/models/request/auth_request.dart';
import 'package:amjad_task/app/data/models/response/user_auth_model.dart';
import 'package:amjad_task/app/data/service/api_service/api_bloc.dart';
import 'package:amjad_task/app/data/service/api_service/request.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import '../../../../core/config/app_service.dart';
import '../../../../data/service/utils/constants.dart';

part 'log_in_event.dart';

part 'log_in_state.dart';

class LogInBloc extends Bloc<LogInEvent, LogInState> {
  bool _hidePassword = true;
  late TextEditingController textEditingControllerUserName;
  late TextEditingController textEditingControllerPassword;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  _init() {
    textEditingControllerPassword = TextEditingController();
    textEditingControllerUserName = TextEditingController();
  }

  LogInBloc() : super(LogInInitial()) {
    _init();
    on<HidePasswordEvent>(_hideShowPassword);
  }

  FutureOr<void> _hideShowPassword(LogInEvent event, Emitter<LogInState> emit) {
    _hidePassword = !_hidePassword;
    emit(HidePasswordState(_hidePassword));
  }

  Request get _loginReq => Request(
        method: MethodHttp.post,
        endPoint: EndPoints.logIn,
        body: LogInRequest(
          password:textEditingControllerPassword.text,
          userName:textEditingControllerUserName.text,
        ).toJson,
      );

  void login<res>(ApiBloc<res> apiBloc) {
    if (!formKey.currentState!.validate()) return;
    apiBloc.add(
      ApiRequestEvent(request: _loginReq),
    );
  }

  void onLoginSuccess(UserAuthModel data, BuildContext context) {
    AppService().setUserData(user: data);
    Future.delayed(
      const Duration(milliseconds: 150),
      () => context.pushReplacementNamed(Routes.homeScreen),
    );
  }

  @override
  Future<void> close() {
    textEditingControllerUserName.dispose();
    textEditingControllerPassword.dispose();
    ApiBloc<UserAuthModel>().close();
    return super.close();
  }
}
