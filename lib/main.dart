import 'package:flutter/cupertino.dart';

import 'app/core/routes/app_pages.dart';
import 'app/my_app.dart';

void main() async {

  runApp(
    MyApp(
      appRoute: AppPages(),
    ),
  );
}